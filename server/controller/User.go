package controller

import (
	"chess/service"
	"chess/utils"
	"fmt"
	"github.com/gin-gonic/gin"
)

var userService *service.UserService

func UserLogin(c *gin.Context) {
	u := utils.ParseUser(c)
	fmt.Println(u)
	loginUser, err := userService.Login(u)
	if err != nil {
		utils.Error(c, -1, err.Error(), "")
	} else {
		//loginUser = utils.ParseRetUser(loginUser)
		utils.Success(c, 0, "登录成功", loginUser)
	}
}

func UserRegister(c *gin.Context) {
	u := utils.ParseUser(c)
	u, err := userService.Register(u)
	if err != nil {
		utils.Error(c, -1, err.Error(), "")
	} else {
		//u = utils.ParseRetUser(u)
		utils.Success(c, 0, "注册成功", u)
	}
}

func init()  {
	userService = new(service.UserService)
}