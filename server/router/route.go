package router

import (
	"chess/controller"
	"chess/websocket"
	"github.com/gin-gonic/gin"
)

func InitRoute(engine *gin.Engine)  {
	//聊天
	engine.GET("/", func(context *gin.Context) {
		context.String(200,"Hello World!")
	})
	//聊天
	engine.GET("/ws",websocket.WS)
	//登录
	engine.POST("/login", controller.UserLogin)
	//注册
	engine.POST("/register", controller.UserRegister)

}