package utils

import (
	"chess/model"
	"regexp"
)

func UserRe(user *model.User) bool {
	return UserNameRe(user.UserName)
}
//手机11位
func PhoneRe(phone string) bool {
	compile := regexp.MustCompile(`^[1]([3-9])[0-9]{9}$`)
	return compile.MatchString(phone)
}

//用户名6-16位 字母数字下划线
func UserNameRe(username string) bool {
	compile := regexp.MustCompile(`^[a-zA-Z0-9_]{3,16}$`)
	return compile.MatchString(username)
}
