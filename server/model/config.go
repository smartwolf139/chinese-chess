package model

type Server struct {
	Port string `json:"port"`
	Mysql MysqlConfig `json:"mysql"`
}
type MysqlConfig struct {
	UserName string `json:"username"`
	PassWord string `json:"password"`
	DbName   string `json:"dbname"`
	CharSet  string `json:"charset"`
	Ip       string `json:"ip"`
	Port     string `json:"port"`
}