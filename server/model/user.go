package model

import "time"

type User struct {
	Id         int64     `json:"id,omitempty" xorm:"not null pk autoincr int(11)" form:"id"`
	UserName   string    `json:"user_name,omitempty" xorm:"varchar(20)" form:"user_name"`
	PassWord   string    `json:"pass_word,omitempty" xorm:"varchar(50)" form:"pass_word"`
	Phone      string    `json:"phone,omitempty" xorm:"varchar(20)" form:"phone"`
	Sex        string    `json:"sex,omitempty" xorm:"varchar(2)" form:"sex"`
	Salt       string    `json:"salt,omitempty" xorm:"varchar(50)" form:"salt"`
	Avatar     string    `json:"avatar,omitempty" xorm:"varchar(200)" form:"avatar"`
	NickName   string    `json:"nick_name,omitempty" xorm:"varchar(20)" form:"nick_name"`
	CreateTime time.Time `json:"create_time,omitempty" xorm:"created" form:"create_time"`
	UpdateTime time.Time `json:"update_time,omitempty" xorm:"updated" form:"update_time"`
	QQ         string    `json:"qq,omitempty" xorm:"varchar(15)" form:"qq"`
	LastIp     string    `json:"last_ip,omitempty" xorm:"varchar(15)" form:"last_ip"`
	Token      string    `json:"token,omitempty" xorm:"varchar(50)" form:"token"`

	//用户状态(0-正常)
	Status     string `json:"status,omitempty" xorm:"varchar(5)" form:"status"`
	PhoneLogin bool   `json:"phone_login,omitempty" xorm:"-"" form:"phone_login"`
}