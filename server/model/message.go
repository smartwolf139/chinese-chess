package model

import "time"

type Message struct {
	Id int64 `json:"id,omitempty" form:"id" xorm:"not null pk autoincr int(11)"` // xorm:"note null pk autoincr int(11)"
	//用户ID
	UserId int64 `json:"user_id,omitempty" form:"user_id" xorm:"int(11)"`
	//接收ID
	DestId int64 `json:"dest_id,omitempty" form:"dest_id" xorm:"int(11)"`
	//操作
	Cmd int64 `json:"cmd,omitempty" form:"cmd" xorm:"int(5)"`
	//数据类型
	Media int `json:"media,omitempty" form:"media" xorm:"int(5)"`
	//消息内容
	Content string `json:"content,omitempty" form:"content" xorm:"varchar(200)"`
	//图片内容
	Pic string `json:"pic,omitempty" form:"pic" xorm:"varchar(100)"`
	//链接内容
	Url string `json:"url,omitempty" form:"url" xorm:"varchar(100)"`
	//描述
	Memo string `json:"memo,omitempty" form:"memo" xorm:"varchar(100)"`
	//附加数据
	Amount int `json:"amount,omitempty" form:"amount" xorm:"int(5)"`
	//状态
	Status int `json:"status,omitempty" form:"status" xorm:"int(5)"`
	//发送时间
	CreateTime time.Time `json:"create_time,omitempty" form:"create_time" xorm:"created"`
	
	//token
	Token string `json:"token"`

	//点击消息
	Click click `json:"click"`
}
type click struct {
	Row int `json:"row"`
	Col int `json:"col"`
	Id int `json:"id"`
}