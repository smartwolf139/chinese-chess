module chess

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-xorm/xorm v0.7.9
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/kr/pretty v0.2.1 // indirect
)
