package global

import (
	"chess/model"
	"sync"
)

var(
	SERVER_CONFIG *model.Server

	//所有用户信息
	ClientMap map[int64]*model.Node
	//全局锁
	Lock      sync.RWMutex
	//临时存放所有用户信息
	AllUserByToken map[string]*model.User
	AllUserById map[int64]*model.User

	//存放匹配的用户
	ClientMatch []string
	//存放开始游戏的用户
	ClientStart map[int64]int64
)

func init() {
	SERVER_CONFIG = new(model.Server)
	ClientMap = make(map[int64]*model.Node)
	AllUserByToken = make(map[string]*model.User)
	AllUserById = make(map[int64]*model.User)
	ClientStart = make(map[int64]int64)
}