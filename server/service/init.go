package service

import (
	"chess/global"
	"chess/model"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"log"
)

var DBEngine *xorm.Engine

func init() {
	str := global.SERVER_CONFIG.Mysql.UserName + ":" + global.SERVER_CONFIG.Mysql.PassWord + "@(" + global.SERVER_CONFIG.Mysql.Ip + ":" + global.SERVER_CONFIG.Mysql.Port + ")/" + global.SERVER_CONFIG.Mysql.DbName + "?charset=" + global.SERVER_CONFIG.Mysql.CharSet
	log.Println(str)
	engine, err := xorm.NewEngine("mysql", str)
	if err != nil {
		log.Fatal(err.Error())
	}
	engine.ShowSQL(true)
	engine.SetMaxOpenConns(2)
	engine.Sync2(new(model.User),new(model.Message))
	DBEngine = engine
	log.Println("database init success")
}
