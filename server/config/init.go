package config

import (
	"bufio"
	"chess/global"
	"encoding/json"
	"log"
	"os"
)

func init()  {
	open, err := os.Open("./config/app.json")
	if err!=nil {
		log.Fatal(err)
	}
	defer open.Close()
	reader := bufio.NewReader(open)
	decoder := json.NewDecoder(reader)
	err = decoder.Decode(&global.SERVER_CONFIG)
	if err!=nil {
		log.Fatal(err)
	}

}