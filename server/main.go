package main

import (
	_ "chess/config"
	"chess/global"
	"chess/router"
	"github.com/gin-gonic/gin"
)

func main()  {
	engine := gin.Default()
	router.InitRoute(engine)

	engine.Run(global.SERVER_CONFIG.Port)
}