# 中国象棋

## 最近更新
解决资源文件中文乱码
服务端新增 python 版本

#### 介绍
中国象棋，支持网络，人机，残局等。

#### 演示视频
https://www.bilibili.com/video/BV18Z4y1F7a1
#### 软件架构
软件架构说明
GoLang，C++,QT，Gin，Mysql

#### 安装教程

1.  git clone https://gitee.com/djac/chinese-chess.git
2.  启动服务端
3.  修改Login.cpp里面的服务器ip地址，启动

#### 使用说明

1.  如果项目有帮助到您，麻烦点个Star吧，谢谢。

#### 项目目录
client:  
>img:图片资源文件  
>pu:棋谱文件  
>res:开局库残局库文件  
>sound:音效文件  
>video:音乐文件  

>Board:初始化文件  
>NetGame:网络模式  
>SignleGame:人机和残局模式  
>Skin:皮肤  
>Step:步  
>Piece:棋子  
>Canju:残局界面  
>Login:主界面  

server:  
>config:  
>controller:  
>global:  
>model:  
>router:  
>service:  
>utils:  
>websockets:  
