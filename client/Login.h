#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QAbstractButton>
#include <QDebug>

#include <SingleGame.h>
#include <NetGame.h>

#include <QMessageBox>

//HTTP
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>

//JSON
#include <QJsonDocument>
#include <QJsonObject>

#include <Skin.h>

#include <CanJu.h>



namespace Ui {
class Login;
}

class Login : public QDialog
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

    QNetworkAccessManager * manager;
    static QString BaseIP;
    static QString BasePort;
    static QString Token;
    static QString UserName;
    static QString GamePattern;

//    static QString Skin;

private slots:

    //HTTP请求完成
    void httpFinished(QNetworkReply *replay);

    void on_loginButton_clicked();

    void on_registerButton_clicked();

    //请求处理函数
    void buttonClick(QString url);

    //跳转函数
    void showMain();

    void on_musicBox_clicked();


    void on_gameBox_currentIndexChanged(const QString &arg1);

private:
    Ui::Login *ui;

};

#endif // LOGIN_H
