#ifndef PIECE_H
#define PIECE_H

#include<QString>
#include<QDebug>

class Piece
{
public:
    Piece();

    enum TYPE{JIANG,CHE,PAO,MA,BING,SHI,XIANG};

    //棋子的行列 id 是否死亡 颜色
    int _x;
    int _y;
    int _id;
    bool _dead;
    bool _red;
    TYPE _type;

    //获取棋子的名字
    QString getName()
    {
        switch (this->_type)
        {
        case JIANG:
            return "将";
        case CHE:
            return "车";
        case PAO:
            return "炮";
        case MA:
            return "马";
        case BING:
            return "兵";
        case SHI:
            return "士";
        case XIANG:
            return "相";
        default:
            return "NULL";
        }
    }

    //获取棋子的名字
    QString getNameEn()
    {
        switch (this->_type)
        {
        case JIANG:
            return "k";
        case CHE:
            return "r";
        case PAO:
            return "c";
        case MA:
            return "n";
        case BING:
            return "p";
        case SHI:
            return "a";
        case XIANG:
            return "b";
        default:
            return "NULL";
        }
    }

    //初始化棋子
    void init(int id)
    {
        //定义棋子位置结构体
        struct {
            int x,y;
            Piece::TYPE type;
        }pos[16]={
        {0,0,Piece::CHE},
        {0,1,Piece::MA},
        {0,2,Piece::XIANG},
        {0,3,Piece::SHI},
        {0,4,Piece::JIANG},
        {0,5,Piece::SHI},
        {0,6,Piece::XIANG},
        {0,7,Piece::MA},
        {0,8,Piece::CHE},

        {2,1,Piece::PAO},
        {2,7,Piece::PAO},
        {3,0,Piece::BING},
        {3,2,Piece::BING},
        {3,4,Piece::BING},
        {3,6,Piece::BING},
        {3,8,Piece::BING}
        };

        _id = id;
        _dead = false;
        //红下黑上
        _red = id>=16;
        //初始化位置和类型
        if(id<16){
            _x=pos[id].x;
            _y=pos[id].y;
            _type=pos[id].type;
        }else{
            _x=9-pos[id-16].x;
            _y=8-pos[id-16].y;
            _type=pos[id-16].type;
        }
//        qDebug()<<id<<_x<<_y<<getName()<<_red ;
    }

    //旋转
    void rotate()
    {
        this->_y = 8-this->_y;
        this->_x = 9-this->_x;
    }
};

#endif // PIECE_H
