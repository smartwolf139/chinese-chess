#include "Skin.h"

QString Skin::GlobalSkin = "经典棋子";
QMediaPlaylist *Skin::Playlist;
QMediaPlayer *Skin::Music;
QMediaPlayer *Skin::eatPlayer;
QMediaPlayer *Skin::goPlayer;
QMediaPlayer *Skin::lsPlayer;
Skin::Skin()
{

}
void Skin::Init()
{
    //播放背景音乐
    Playlist = new QMediaPlaylist;
    Playlist->addMedia(QUrl("qrc:/video/bg1.mp3"));
    Playlist->setPlaybackMode(QMediaPlaylist::Loop);

    Music = new QMediaPlayer;
    Music->setPlaylist(Playlist);
    Music->play();

    //eat
    eatPlayer = new QMediaPlayer;
    eatPlayer->setMedia(QUrl("qrc:/sound/eat.mp3"));
    //go
    goPlayer = new QMediaPlayer;
    goPlayer->setMedia(QUrl("qrc:/sound/go.mp3"));
    //ls
    lsPlayer = new QMediaPlayer;

}
