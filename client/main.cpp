#include <QApplication>
#include "SingleGame.h"
#include "NetGame.h"
#include <QMessageBox>
#include <QPushButton>
#include <QScreen>
#include "Login.h"

int main(int argc,char* argv[])
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication app(argc,argv);
    Login *login = new Login;
    login->show();
    return app.exec();
}
