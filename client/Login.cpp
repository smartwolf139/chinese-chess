#include "Login.h"
#include "ui_Login.h"

QString Login::Token = "";
QString Login::UserName = "";
QString Login::BaseIP = "127.0.0.1";
//QString Login::BaseIP = "114.116.73.86";
QString Login::BasePort = "8080";
QString Login::GamePattern = "人机对战";
//QString Login::Skin = "经典皮肤";

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    //构造函数
    manager = new QNetworkAccessManager();
    //连接槽函数
    QObject::connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(httpFinished(QNetworkReply*)));
    //查看支持协议
    qDebug()<<manager->supportedSchemes();

    Skin::Init();


    //隐藏残局选项
    ui->label_5->setHidden(true);
    ui->roleBox->setHidden(true);


    //test

    //end
}

Login::~Login()
{
    delete ui;
}

void Login::on_loginButton_clicked()
{
    qDebug()<<"登录";
    //处理游戏模式和皮肤选择
    GamePattern=ui->gameBox->currentText();
    if(GamePattern=="网络对战"){
        buttonClick("/login");
    }else{
        showMain();
    }




//    this->hide();
//    NetGame *board = new NetGame;
//    board->show();

}

void Login::on_registerButton_clicked()
{
    qDebug()<<"注册";
    GamePattern=ui->gameBox->currentText();
    if(GamePattern=="网络对战"){
        buttonClick("/register");
    }else{
        showMain();
    }
}

void Login::httpFinished(QNetworkReply *replay)
{
    QByteArray data = replay->readAll();
    QJsonParseError *error=new QJsonParseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data,error);
    if(error->error!=QJsonParseError::NoError)
    {
        qDebug()<<"parseJson:"<<error->errorString();
        return;
    }
    QJsonObject obj = jsonDoc.object();
    QString token = obj["data"].toObject()["token"].toString();
    QString msg = obj["msg"].toString();
    int code = obj["code"].toInt();
    qDebug()<<token<<msg<<code;
    QMessageBox::information(NULL,"提示",msg);
    if(code==0){
         Login::Token = token;
         Login::UserName = obj["data"].toObject()["user_name"].toString();
         //跳转界面
         showMain();
    }
}
void Login::buttonClick(QString url)
{
    QString username = ui->usernameEdit->text();
    QString password = ui->passwordEdit->text();
    if(username==""||password==""){
        QMessageBox::warning(this,QStringLiteral("提示"),QStringLiteral("参数不正确"));
        return;
    }
    if(username.length()<3||password.length()<3){
        QMessageBox::warning(this,QStringLiteral("提示"),QStringLiteral("长度不正确"));
        return;
    }
    qDebug()<<"登录"<<username<<password<<"http://"+Login::BaseIP+":"+Login::BasePort+url;
    QNetworkRequest req;
    req.setUrl(QUrl("http://"+Login::BaseIP+":"+Login::BasePort+url));
    req.setRawHeader("Content-Type","application/json");
    QString data="{\"user_name\":\""+username+"\",\"pass_word\":\""+password+"\"}";
    manager->post(req,data.toUtf8());
}

void Login::showMain()
{
    //处理游戏模式和皮肤选择
    GamePattern=ui->gameBox->currentText();
    Skin::GlobalSkin = ui->skinBox->currentText();

    Board *board;
    if(GamePattern=="人机对战"){
        board = new SingleGame;
    }else if(GamePattern=="网络对战"){
        board = new NetGame;
    }else if(GamePattern=="残局练习"){
        CanJu *canju = new CanJu;
        canju->role=ui->roleBox->currentText();
        canju->setStyleSheet("QHeaderView::section{background-color:rgba(255,255,255,0)}\n#CanJu{background-image: url(:/img/role/hx.png);}\nfont: 14pt \"楷体\";");



        canju->show();
        return;
    }

//    this->hide();
//    SingleGame *board = new SingleGame;
//    NetGame *board = new NetGame;
    //设置窗口最小高度
//    board->setMinimumWidth(1000);
//    board->setMaximumWidth(1000);
//    board->setMinimumHeight(680);
//    board->setMaximumHeight(680);
    //设置窗口标题
    board->setWindowTitle("小象棋-"+GamePattern);
    board->setObjectName("board");
    qDebug()<<board->objectName();
    //设置背景
    board->setAutoFillBackground(true);

    QPalette palette;
    QPixmap p = QPixmap(":/img/background1.jpeg");

    palette.setBrush(QPalette::Background, QBrush(p));
    palette.setBrush(board->backgroundRole(), QBrush(p));
//    board->setStyleSheet("#board{background-image: url(:/img/inputChat.png);}");

    board->setPalette(palette);
    board->show();
}

void Login::on_musicBox_clicked()
{
    if(ui->musicBox->isChecked()==true){
        Skin::Music->play();
    }else{
        Skin::Music->stop();
    }
}

void Login::on_gameBox_currentIndexChanged(const QString &arg1)
{
    if(arg1!="残局练习"){
        ui->roleBox->setHidden(true);
        ui->label_5->setHidden(true);
    }else{
        ui->roleBox->setHidden(false);
        ui->label_5->setHidden(false);
    }
}
