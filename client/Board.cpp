#include "Board.h"
#include <QPainter>
#include <QMouseEvent>

Board::~Board()
{
    qDebug()<<"关闭程序";
}

void Board::closeEvent ( QCloseEvent * e )
{

}

Board::Board(QWidget *parent) : QWidget(parent)
{
    //初始化界面大小
    _d=60;
    //初始化棋子
    for (int i=0;i<32;i++) {
        _p[i].init(i);
    }
    _selectid=-1;
    _bRedRun=true;
    _bSide=true;
    //绘制界面
    isUpdate=true;
    //是否已经开始游戏
    isStart=false;
    //是否胜利
    isWin=-1;
    //初始化最后一步
    lastStep=NULL;


    this->setMinimumWidth(_d/10*167);
    this->setMaximumWidth(_d/10*167);
    this->setMinimumHeight(_d/10*113);
    this->setMaximumHeight(_d/10*113);

    //初始化快捷消息
    quickList.append("1.快点吧我等的花儿都谢了");
    quickList.append("2.淡定淡定让我想想");
    quickList.append("3.怎么又掉线了");
    quickList.append("4.你这么厉害你咋不上天呢");
    quickList.append("5.七分靠运气三分靠技术");
    quickList.append("6.不想吃将的兵不是好棋手");
    quickList.append("7.我的天呐我忍不了");
    quickList.append("8.冲动是魔鬼冷静");
    quickList.append("9.交个朋友吧");
    quickList.append("10.别再跑啦到我碗里来");
    quickList.append("11.一不小心我就赢啦");
    quickList.append("12.难道这就是失败的滋味");
    quickList.append("13.赢了钱别走");
    quickList.append("14.再来一局呗");
    quickList.append("15.我走啦拜拜");
    //初始化男声
    isMan="Man";



    showButton();


    //test
    isPlay=true;
    redOpacity = new QGraphicsOpacityEffect(lab);
    lab->setGraphicsEffect(redOpacity);
    redOpacity->setOpacity(1);
    animation = new QPropertyAnimation;
    animation->setTargetObject(redOpacity);
    animation->setPropertyName("opacity");
//    animation->setStartValue(0);
//    animation->setEndValue(1);
//    animation->setDuration(500);
//    animation->start();
    connect(animation,SIGNAL(finished()),this,SLOT(animationFinished()));
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(handleTimeout()));
    //end

}

void Board::paintEvent(QPaintEvent *)
{

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    QRect rect2 = QRect(0,0,1000,1000);
    painter.drawPixmap(rect2,QPixmap::fromImage(QImage(":/img/Common_Bg_Main.jpg")));
    //end

    //画棋盘
    //横轴
    int d = _d;
    painter.save();
    for(int i=1;i<=10;i++)
    {
        //在x(d,i*d)点到y(9*d,i*d)的一条直线
        painter.drawLine(QPoint(d,i*d),QPoint(9*d,i*d));
    }
    //纵轴
    for(int i=1;i<10;i++)
    {
        //在x(i*d,d)点到y(i*d,10*d)的一条直线
        if(i==1||i==9){
            painter.drawLine(QPoint(i*d,d),QPoint(i*d,10*d));
        }else{
            painter.drawLine(QPoint(i*d,d),QPoint(i*d,5*d));
            painter.drawLine(QPoint(i*d,6*d),QPoint(i*d,10*d));
        }
    }
    //九宫格
    painter.drawLine(QPoint(4*d,d),QPoint(6*d,3*d));
    painter.drawLine(QPoint(4*d,3*d),QPoint(6*d,d));

    painter.drawLine(QPoint(4*d,8*d),QPoint(6*d,10*d));
    painter.drawLine(QPoint(4*d,10*d),QPoint(6*d,8*d));

    painter.restore();

    //绘制楚河，汉界
    QRect rect = QRect(_d,_d*5,_d*8,_d);
    QFont qf;
    qf.setPointSize(_d/10*4.5);
    qf.setBold(true);
    qf.setFamily("KaiTi");
    painter.setFont(qf);
    painter.drawText(rect,"楚 河        汉 界",QTextOption(Qt::AlignCenter));

    //绘制兵和炮折线

    if(lastStep!=NULL){
        //绘制下棋位置框框
        painter.setPen(Qt::red);
        QRect rect1 = QRect(lastStep->_colto*_d+_d/2,lastStep->_rowto*_d+_d/2,_d-2,_d-2);
        painter.drawRect(rect1);
        rect1 = QRect(lastStep->_colfrom*_d+_d/2,lastStep->_rowfrom*_d+_d/2,_d-2,_d-2);
        painter.drawRect(rect1);
    }

    //绘制32个棋子
    painter.save();
    for(int i=0;i<32;i++)
    {
        drawPiece(painter,i);
    }
    painter.restore();



}

void Board::drawPiece(QPainter& painter,int id)
{
    //判断棋子是否被吃
    if(_p[id]._dead)
        return;
    //得到点
    QPoint c = center(id);
    //得到矩形
    QRect rect = QRect(c.x()-_d/2,c.y()-_d/2,_d,_d);
    //设置画刷
    if(id==_selectid){
        painter.setBrush(QBrush(Qt::gray));
    }else{
        painter.setBrush(QBrush(Qt::yellow));
    }
    //设置字的颜色
    painter.setPen(Qt::black);
    if(_p[id]._red)
        painter.setPen(Qt::red);

    if (Skin::GlobalSkin=="经典棋子"){
        //画圆
        painter.drawEllipse(c,_d/2,_d/2);
        //画字
        QFont font;
        font.setPointSize(_d/2);
        painter.setFont(font);
        painter.drawText(rect,_p[id].getName(),QTextOption(Qt::AlignCenter));
    }else if(Skin::GlobalSkin=="炫酷棋子"){
        if(_selectid==id){
            //放大选中的矩形
            rect = QRect(c.x()-_d/1.8,c.y()-_d/1.8,_d*1.2,_d*1.2);
        }

        //图片
        if(_p[id]._red){
            QImage image(QString(":/img/w"+_p[id].getNameEn()+".png"));
            painter.drawPixmap(rect,QPixmap::fromImage(image));
        }else{
            QImage image(QString(":/img/b"+_p[id].getNameEn()+".png"));
            painter.drawPixmap(rect,QPixmap::fromImage(image));
        }
    }

//    QImage image(QString(":/img/红兵.png"));
//    painter.drawPixmap(rect,QPixmap::fromImage(image));
}

QPoint Board::center(int x,int y)
{
    QPoint ret;
    ret.rx()=(y+1)* _d;
    ret.ry()=(x+1)* _d;
    return ret;
}

QPoint Board::center(int id)
{
    return center(_p[id]._x,_p[id]._y);
}

void Board::click(int id,int x,int y)
{
    qDebug()<<"开始点击";
    //判断当前位置有没有棋子
    int i;
    for(i=0;i<32;i++)
    {
        if(_p[i]._x==x && _p[i]._y==y && _p[i]._dead==false)
        {
            break;
        }
    }
    int clickid=-1;
    //没有点到棋子
    if(i<32)
        clickid=i;

    if(_selectid==-1)
    {
        if(clickid!=-1)
        {
            if(_bRedRun == _p[clickid]._red){
                //拿子
                _selectid=clickid;
                //重新绘制
                update();
                //播放音效
                Skin::lsPlayer->setMedia(QUrl("qrc:/sound/select.mp3"));
                Skin::lsPlayer->play();
            }
        }
    }else{
        //判断是否符合规则
        if(canMove(_selectid,x,y,clickid))
        {
            //保存步数
            Step *step=new Step;
            step->_moveid = _selectid;
            step->_killid = clickid;
            step->_rowfrom = _p[_selectid]._x;
            step->_colfrom=_p[_selectid]._y;
            step->_rowto=x;
            step->_colto=y;

            //更新最后一步
            lastStep=step;

            saveStep(_selectid,clickid,x,y,regretSteps);
            //saveStep(step,regretSteps);
            qDebug()<<step->_moveid<<"吃1"<<step->_killid<<"row from"<<step->_rowfrom<<"col from"<<step->_colfrom<<"row to"<<step->_rowto<<"col to"<<step->_colto;
            //落子
            _p[_selectid]._x=x;
            _p[_selectid]._y=y;
            if(clickid!=-1){
                //播放音效
                Skin::eatPlayer->play();
                //end
                _p[clickid]._dead=true;
                playAnimation("eat");
                //判断吃的是否是将
                if(_p[clickid]._type==Piece::JIANG){
                    gameOver();
                }
                //end
            }else{
                Skin::goPlayer->play();
            }
            _selectid=-1;
            isShowButton(_bRedRun==_bSide);
            _bRedRun=!_bRedRun;
            update();
        }
    }
}

void Board::isShowButton(bool isShow)
{
    qDebug()<<"Board isShowButton";
    //是否禁止悔棋按钮
    if(regretSteps.size()<2){
        regretButton->setEnabled(false);
        return;
    }
    if(isShow){
        regretButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: rgb(85, 255, 0);font-weight: bold;border-image: url(:/img/btn-bg.png);");
    }else{
        regretButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");
    }
    regretButton->setEnabled(isShow);
}

void Board::mousePressEvent(QMouseEvent *event)
{
    if(isStart==false){
        return;
    }
    QPoint pt = event->pos();
    //判断是否是棋盘外
    if(pt.x()>_d*10){
        update();
        return;
    }
    //坐标转换象棋的行列
    int x,y;
    bool bRet = getXY(pt,x,y);
    //点到棋盘外
    if(bRet==false)
        return;
    int id = getPieceId(x,y);

    click(id,x,y);


}

bool Board::getXY(QPoint pt,int& x,int& y)
{
    for(x=0;x<=9;x++)
    {
        for(y=0;y<=8;y++)
        {
            QPoint c = center(x,y);
            int dx = qAbs(c.x()-pt.x());
            int dy = qAbs(c.y()-pt.y());
            if(dx<_d/2 && dy<_d/2){
                return true;
            }
        }
    }
    return false;
}

bool Board::canMove(int moveid,int row,int col,int killid)
{
    //判断是否是己方
    if(sameColor(moveid,killid)){
        //更换选择的棋子
        _selectid=killid;
//        qDebug()<<(_p[_selectid].getName())<<_bRedRun<<_bSide;
        update();
        //播放音效
        Skin::lsPlayer->setMedia(QUrl("qrc:/sound/select.mp3"));
        Skin::lsPlayer->play();
        return false;
    }
    //判断类型
    switch (_p[moveid]._type) {
    case Piece::JIANG:
        return canMoveJ(moveid,row,col,killid);
        break;
    case Piece::SHI:
        return canMoveS(moveid,row,col,killid);
        break;
    case Piece::CHE:
        return canMoveC(moveid,row,col,killid);
        break;
    case Piece::MA:
        return canMoveM(moveid,row,col,killid);
        break;
    case Piece::PAO:
        return canMoveP(moveid,row,col,killid);
        break;
    case Piece::XIANG:
        return canMoveX(moveid,row,col,killid);
        break;
    case Piece::BING:
        return canMoveB(moveid,row,col,killid);
        break;
    }
    return true;
}

bool Board::canMoveJ(int moveid,int row,int col,int killid)
{
    //先判断是否名将
    if(killid!=-1){
        if(killid==4||killid==20){
            int ret = getPieceCountByLine(_p[moveid]._x,_p[moveid]._y,row,col);
            if(ret==0){
                return true;
            }
        }
    }
    //end

    /*
     * 1.首先目标要在九宫格内
     * 2.移动的步长为1
    */
    //限制九宫格
    if(isBottomSide(moveid)){
        if(row>2)return false;
    }else{
        if(row<7)return false;
    }
    if(col<3 || col>5) return false;

    int dr = _p[moveid]._x - row;
    int dc = _p[moveid]._y - col;
    if(qAbs(dr)+qAbs(dc)==1)
        return true;
    return false;
}
bool Board::canMoveS(int moveid,int row,int col,int killid)
{
    if(isBottomSide(moveid)){
        if(row>2)return false;
    }else{
        if(row<7)return false;
    }
    if(col<3 || col>5) return false;

    int dr = _p[moveid]._x - row;
    int dc = _p[moveid]._y - col;
    if(qAbs(dr)==1&&qAbs(dc)==1)
        return true;
    return false;
}
bool Board::canMoveC(int moveid,int row,int col,int)
{
    int ret = getPieceCountByLine(_p[moveid]._x,_p[moveid]._y,row,col);
    if(ret==0){
        return true;
    }
    return false;
}
bool Board::canMoveM(int moveid,int row,int col,int)
{
    int dr = _p[moveid]._x - row;
    int dc = _p[moveid]._y - col;
    if(qAbs(dr)==2&&qAbs(dc)==1)
    {
        //判断是否绊马蹄
        dr = (_p[moveid]._x + row)/2;
        if(getPieceId(dr,_p[moveid]._y)==-1){
            return true;
        }
    }else if(qAbs(dr)==1&&qAbs(dc)==2){
        //判断是否绊马蹄
        dc = (_p[moveid]._y + col)/2;
        if(getPieceId(_p[moveid]._x,dc)==-1){
            return true;
        }
    }
    return false;
}
bool Board::canMoveP(int moveid,int row,int col,int killid)
{
    //得到两点直线中间的棋子数
    int ret = getPieceCountByLine(_p[moveid]._x,_p[moveid]._y,row,col);
    if(killid==-1)
    {
        if(ret==0){
            return true;
        }
    }else{
        if(ret==1){
            return true;
        }
    }
    return false;
}
bool Board::canMoveX(int moveid,int row,int col,int)
{
    //限制河
    if(isBottomSide(moveid)){
        if(row>4)return false;
    }else{
        if(row<5)return false;
    }
    int dr = _p[moveid]._x - row;
    int dc = _p[moveid]._y - col;
    if(qAbs(dr)==2&&qAbs(dc)==2)
    {
        //判断象眼
        dr = (_p[moveid]._x + row)/2;
        dc = (_p[moveid]._y + col)/2;
        //判断象眼是否有棋子
        if(getPieceId(dr,dc)==-1){
            return true;
        }
    }
    return false;
}
bool Board::canMoveB(int moveid,int row,int col,int killid)
{
    int dr = _p[moveid]._x - row;
    int dc = _p[moveid]._y - col;
    if(qAbs(dr)+qAbs(dc)!=1){
        return false;
    }
//    if(!_p[moveid]._red){
    if(isBottomSide(moveid)){

        //黑兵
        //判断是否后退
        if(row<_p[moveid]._x){
            return false;
        }
        //判断是否过河
        if(row>4){
            return true;
        }else{
            if(_p[moveid]._y==col){
                return true;
            }
        }
    }else{
        //红兵
        //判断是否后退
        if(row>_p[moveid]._x){
            return false;
        }
        //判断是否过河
        if(row<5){
            return true;
        }else{
            if(_p[moveid]._y==col){
                return true;
            }
        }
    }
    return false;
}

int Board::getPieceId(int dr,int dc)
{
    for (int i=0;i<32;i++) {
        if(_p[i]._x==dr && _p[i]._y==dc &&!_p[i]._dead){
            return i;
        }
    }
    return -1;
}

int Board::getPieceCountByLine(int row1,int col1,int row2,int col2)
{
    int ret = 0;
    //不符合规则
    if(row1!=row2&&col1!=col2)
    {
        return -1;
    }
    if(row1==row2&&col1==col2)
    {
        return -1;
    }

    if(row1==row2)
    {
        int max = col1<col2?col2:col1;
        int min = col1<col2?col1:col2;
        for(int col = min+1;col<max;col++)
        {
            if(getPieceId(row1,col)!=-1){
                //qDebug()<<"找到点1"<<row1<<col<<_p[getPieceId(row1,col)]._red;
                ret++;
            }
        }
    }else{
        int max = row1<row2?row2:row1;
        int min = row1<row2?row1:row2;
        for(int row = min+1;row<max;row++)
        {
            if(getPieceId(row,col1)!=-1){
                //qDebug()<<"找到点2"<<row<<col1<<_p[getPieceId(row,col1)]._red;
                ret++;
            }
        }
    }
    return ret;
}

bool Board::sameColor(int moveid,int killid)
{
    if(moveid==-1||killid==-1){
        return false;
    }
    return _p[moveid]._red==_p[killid]._red;
}
void Board::killPiece(int id)
{
    if(id==-1){
        return;
    }
    _p[id]._dead=true;
}
void Board::revivePiece(int id)
{
    if(id==-1){
        return;
    }
    _p[id]._dead=false;
}
void Board::movePiece(int moveid,int row,int col)
{
    _p[moveid]._x = row;
    _p[moveid]._y = col;

    _bRedRun = !_bRedRun;
}
void Board::movePiece(int moveid,int killid,int row,int col)
{
    //保存步数
    //吃掉棋子
    killPiece(killid);
    //移动
    movePiece(moveid,row,col);
}
void Board::saveStep(int moveid,int killid,int row,int col,QVector<Step*>& steps)
{
    Step * step = new Step;
    step->_moveid=moveid;
    step->_killid=killid;
    step->_rowfrom=_p[moveid]._x;
    step->_colfrom=_p[moveid]._y;
    step->_rowto=row;
    step->_colto=col;
    steps.append(step);
//    qDebug()<<_p[moveid].getName()<<"吃"<<_p[killid].getName();
}
//void Board::saveStep(Step *step,QVector<Step*>& steps)
//{
//    steps.append(step);
//}

void Board::showButton()
{
    //悔棋按钮
    regretButton=new QPushButton("悔棋", this);
    regretButton->move(_d*10,_d+40);
    regretButton->resize(_d+40,40);
    regretButton->setEnabled(false);
    regretButton->setFlat(true);
    regretButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");
    QObject::connect(regretButton, SIGNAL(clicked()), this, SLOT(RegretClick()));
    //开始游戏
    startButton=new QPushButton("开始游戏", this);
    startButton->move(_d*10,_d*2+35);
    startButton->resize(_d+40,40);
    startButton->setFlat(true);
    startButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: rgb(85, 255, 0);font-weight: bold;border-image: url(:/img/btn-bg.png);");
    QObject::connect(startButton, SIGNAL(clicked()), this, SLOT(StartClick()));
    //再来一局
    againButton=new QPushButton("再来一局", this);
    againButton->move(_d*10,_d*3+30);
    againButton->resize(_d+40,40);
    againButton->setFlat(true);
    againButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");
    againButton->setEnabled(false);
    QObject::connect(againButton, SIGNAL(clicked()), this, SLOT(AgainClick()));
    //播放音乐
    musicCheckBox=new QCheckBox("播放音乐", this);
    musicCheckBox->move(_d*10+20,_d-10);
    musicCheckBox->resize(_d+40,40);
    musicCheckBox->setChecked(true);
    QObject::connect(musicCheckBox, SIGNAL(clicked()), this, SLOT(MusicClick()));
    //动画标签
    lab=new QLabel(this);
    lab->move(_d/10*167/6,_d/10*113/2.5);
    lab->resize(330,100);
    lab->setStyleSheet("border-image: url(:/img/black-win.png);");
    lab->setHidden(true);

    //日志窗口
//    logEdit=new QTextEdit(this);
//    logEdit->resize(_d*4,_d*3);
//    logEdit->move(_d*12,50);
//    logEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //设置透明
//    QPalette pl = logEdit->palette();
//    pl.setBrush(QPalette::Base,QBrush(QColor(255,0,0,0)));
//    logEdit->setPalette(pl);
//    logEdit->setFontPointSize(14);

    //聊天面板
    chatEdit=new QPlainTextEdit(this);
    chatEdit->resize(305,302);
    chatEdit->move(630,260);
    chatEdit->setFocusPolicy(Qt::NoFocus);
    chatEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    chatEdit->setStyleSheet("background-image: url(:/img/chatbg.png);background-repeat: no-repeat;padding: 5px 20px;font: 12pt \"楷体\";color: rgb(0, 0, 0);border:0;");

    //快捷聊天面板
    quickChatList=new QListWidget(this);
    quickChatList->resize(206,200);
    quickChatList->move(750,50);
    quickChatList->setFocusPolicy(Qt::NoFocus);
    quickChatList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    quickChatList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    quickChatList->setStyleSheet("background-image: url(:/img/inner.png);padding: 1px 2px;font: 12pt \"楷体\";color: rgb(0, 0, 0);border-left:2px solid transparent;border-right:2px solid transparent;border-image: url(:/img/border.png) 0 1 repeat;");
    quickChatList->addItems(quickList);
    QObject::connect(quickChatList,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(quickDoubleClicked(QModelIndex)));


    //聊天输入
    chatLine=new QLineEdit(this);
    chatLine->installEventFilter(this);
    chatLine->resize(306,49);
    chatLine->move(630,580);
    chatLine->setStyleSheet("background-image: url(:/img/inputChat.png);font: 14pt \"楷体\";border: 0;background-color: rgba(255,255,255,0);padding-left:20px;");
}

void Board::RegretClick()
{
//    regretButton->setEnabled(false);
//    logEdit->append("开始悔棋");
    qLog("系统:不允许悔棋");
    return;
    if(regretSteps.size()<2){
        qLog("系统:没有棋可悔");
        return;
    }
    //取出步数
    Step *step;
    step = regretSteps.back();
    regretSteps.removeLast();
    //复活棋子
    revivePiece(step->_killid);
    movePiece(step->_moveid,step->_rowfrom,step->_colfrom);
    qDebug()<<"悔棋"<<step->_moveid<<"吃"<<step->_killid<<"row from"<<step->_rowfrom<<"col from"<<step->_colfrom<<"row to"<<step->_rowto<<"col to"<<step->_colto;
    step = regretSteps.back();
    regretSteps.removeLast();
    //复活棋子
    revivePiece(step->_killid);
    movePiece(step->_moveid,step->_rowfrom,step->_colfrom);
    update();
//    regretButton->update();
//    logEdit->update();
//    regretButton->setEnabled(true);
}

void Board::StartClick()
{
//    qDebug()<<"开始游戏"<<isStart;
    isStart=true;
    //执黑棋
//    _bRedRun=true;
//    if(_bRedRun)
//    {
//        for(int i=0; i<32; ++i)
//        {
//            _p[i].rotate();
//        }
//    }
    update();
}
void Board::AgainClick()
{
    qDebug()<<"再来一局";
    //初始化
    //初始化棋子
    for (int i=0;i<32;i++) {
        _p[i].init(i);
    }
    _selectid=-1;
    _bRedRun=true;
    _bSide=true;
    isStart=false;
    isWin=-1;
    lastStep=NULL;
    while(regretSteps.count())
    {
        regretSteps.removeLast();
    }
    regretButton->setEnabled(false);
    startButton->setEnabled(true);
    againButton->setEnabled(false);

    regretButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");
    startButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: rgb(85, 255, 0);font-weight: bold;border-image: url(:/img/btn-bg.png);");
    againButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");

    update();
}

void Board::MusicClick()
{
    if(musicCheckBox->isChecked()==true){
        Skin::Music->play();
    }else{
        Skin::Music->stop();
    }
}

bool Board::isBottomSide(int id)
{
    return _bSide == _p[id]._red;
}

void Board::qLog(QString msg)
{
    if(msg.size()<1){
        return;
    }
    chatEdit->appendPlainText(msg);
//    logEdit->append(msg);
//    QString tx = logEdit->toPlainText();
//    if(tx.length()>1000){
//        logEdit->setText(tx.mid(900,100));
//    }
}
bool Board::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == chatLine) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if(keyEvent->key()==Qt::Key_Return){
                //发送消息
                sendMsg();
                return true;
            }
            return QWidget::eventFilter(obj, event);;
        } else {
            return QWidget::eventFilter(obj, event);
        }
    } else {
        // pass the event on to the parent class
        return QWidget::eventFilter(obj, event);
    }
}
void Board::sendMsg()
{
    QString text=chatLine->text().trimmed();
    chatLine->setText("");
    if(text.length()==0){
        return;
    }
    addMsg("我:"+text);
}
void Board::isPlaySound(QString content)
{

}
QString Board::getMoveName(Step *step)
{
    if(isWin!=-1){
        return "";
    }
    QString mumTo[10]={"一","二","三","四","五","六","七","八","九","十"};
    QString h;
    Piece::TYPE type = _p[step->_moveid]._type;
    h=_p[step->_moveid].getName();
    h+= mumTo[step->_colfrom];

    if (step->_rowfrom > step->_rowto) {
        h+= "退";
        if (type == Piece::MA || type == Piece::SHI || type == Piece::XIANG){
            h+= mumTo[step->_colto];
        }else{
            h+= mumTo[step->_rowfrom-step->_rowto];
        }
    }else if (step->_rowfrom < step->_rowto) {
        h+= "进";
        if (type == Piece::MA || type == Piece::SHI || type == Piece::XIANG){
            h+= mumTo[step->_colto];
        }else{
            h+= mumTo[step->_rowto-step->_rowfrom];
        }
    }else {
        h+= "平";
        h+= mumTo[step->_colto];
    }
    return h;
}
void Board::quickDoubleClicked(const QModelIndex &index)
{
    QString text=quickList.at(index.row());
    text=text.mid(2,text.size()-2);
    if(text.at(0)=='.')
    {
        text=text.mid(1,text.size()-1);
    }
    addMsg("我:"+text);
//    qDebug()<<"双击"<<index.row()<<quickList.at(index.row());
}
void Board::addMsg(QString content)
{
    chatEdit->appendPlainText(content);
    QStringList list = content.split(":");
    if(list.size()<2){
        return;
    }
    QString text=list[1],tmp;

    int flag=0;
    flag=quickList.indexOf(text);
    for(int i=0;i<quickList.size();i++)
    {
        tmp=quickList.at(i);
        if(tmp.indexOf(text)>0){
            flag=i+1;
            break;
        }
    }
    if(flag!=0){
        //播放音乐
        playSound(flag);
    }
}
void Board::playSound(int flag)
{
    QString text="100";
    if(flag>=10){
        text="10";
    }
    Skin::lsPlayer->setMedia(QUrl("qrc:/sound/ffq"+isMan+"/"+text+QString::number(flag)+".mp3"));
    Skin::lsPlayer->play();
    if(isMan=="Man"){
        isMan="Women";
    }else{
        isMan="Man";
    }
}
void Board::gameOver()
{
    //判断输赢
    int flag=-1;
    for(int i =0;i<32;i++)
    {
        if(!_p[i]._dead){
            continue;
        }
        if(_p[i]._type==Piece::JIANG){
            flag=i;
            break;
        }
    }
    if(flag==-1){
        return;
    }

    isStart=false;
    regretButton->setEnabled(false);
    regretButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: gray;font-weight: bold;border-image: url(:/img/btn-bg.png);");

    int ind;
    if(_bSide){
        //黑方
        if(_p[flag]._red){
            ind=1;
        }else{
            ind=0;
        }
    }else{
        //红方
        if(!_p[flag]._red){
            ind=1;
        }else{
            ind=0;
        }
    }

    if(_p[4]._dead){
        playAnimation("red-win");
    }else{
        playAnimation("black-win");
    }

    isWin=ind;
    if(ind==1){
        //胜利
        qLog("系统:恭喜你，你赢了");
        Skin::lsPlayer->setMedia(QUrl("qrc:/sound/gamewin.mp3"));
    }else{
        //失败
        qLog("系统:很遗憾，你输了");
        Skin::lsPlayer->setMedia(QUrl("qrc:/sound/gamelose.mp3"));
    }
    Skin::lsPlayer->play();
    againButton->setEnabled(true);
    againButton->setStyleSheet("border:1px solid gray;font: 14pt \"楷体\";color: rgb(85, 255, 0);font-weight: bold;border-image: url(:/img/btn-bg.png);");
}

void Board::animationFinished()
{
    qDebug()<<"动画结束";
    if(isPlay==true){
        timer->start(1000);
        isPlay=false;
    }
}
void Board::handleTimeout()
{
    isPlay=false;
    animation->setStartValue(1);
    animation->setEndValue(0);
    animation->setDuration(500);
    animation->start();
    timer->stop();
}
void Board::playAnimation(QString name)
{
    if(name=="eat"){
        lab->resize(80,80);
        lab->move(_d/10*167/4+10,_d/10*113/2.5+15);
    }else{
        lab->resize(330,100);
        lab->move(_d/10*167/6,_d/10*113/2.5);
    }
    isPlay=true;
    lab->setHidden(false);
    //black-win
    lab->setStyleSheet("border-image: url(:/img/"+name+".png);");
    animation->setStartValue(0);
    animation->setEndValue(1);
    animation->setDuration(300);
    qDebug()<<"动画开始";
    animation->start();
}
