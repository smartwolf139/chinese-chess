#include "CanJu.h"
#include "ui_CanJu.h"

CanJu::CanJu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CanJu)
{
    ui->setupUi(this);

    initCanJu();
}

CanJu::~CanJu()
{
    delete ui;
}

void CanJu::initCanJu()
{
    QString displayString;
    QFile file(":/pu/yshk.pu");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"Can't open the file!"<<endl;
    }
    qDebug()<<"开始读取[渊深海阔]棋谱";
    QTextStream in(&file);
    in.setCodec("GBK");
    displayString=in.readAll();
    file.close();
    QString ls;
    QStringList lsList=displayString.split("\n");
    QStringList tmp;
    for(int i=0,j=0;i<lsList.size();i++)
    {
        ls=lsList.at(i);
        tmp=ls.split(" ");
        if(tmp.size()<4){
            qDebug()<<"解析错误:"<<ls;
            continue;
        }
        canJuMap.insert(tmp[0],tmp[1]+"----"+tmp[2]+"----"+tmp[3]);
        //更新到表格中
        ui->tableWidget->insertRow(j);
        ui->tableWidget->setItem(j,0,new QTableWidgetItem(tmp[0]));
        ui->tableWidget->setItem(j,1,new QTableWidgetItem(tmp[3]));
        j=j+1;
    }
//    qDebug()<<"开局库"<<openList.at(0)<<"。";
}

void CanJu::on_tableWidget_doubleClicked(const QModelIndex &index)
{
    QString text=ui->tableWidget->item(index.row(),0)->text();
    if(!canJuMap.contains(text)){
        qDebug()<<"读取棋谱错误，请检查棋谱";
        return;
    }
    QStringList ls = canJuMap[text].split("----");
    if(ls.size()<3){
        return;
    }
    SingleGame *board = new SingleGame;
    board->puName=text;
    board->location=ls.at(0);
    board->moveList=ls.at(1);
    board->result=ls.at(2);
    board->startCanJu();
    board->show();
}
