HEADERS += \
    Board.h \
    Piece.h \
    SingleGame.h \
    Step.h \
    NetGame.h \
    Login.h \
    Skin.h \
    CanJu.h

SOURCES += \
    Board.cpp \
    Piece.cpp \
    SingleGame.cpp \
    Step.cpp \
    main.cpp \
    NetGame.cpp \
    Login.cpp \
    Skin.cpp \
    CanJu.cpp


QT += widgets gui network websockets multimedia

RESOURCES += \
    img/img.qrc \
    video/video.qrc \
    sound/sound.qrc \
    res/res.qrc \
    pu/pu.qrc

FORMS += \
    Login.ui \
    CanJu.ui
RC_ICONS = icon.ico
