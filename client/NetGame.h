#ifndef NETGAME_H
#define NETGAME_H

#include "Board.h"
#include <Login.h>

#include <QWebSocket>
#include <QHostAddress>

#include <QTimer>

#include <QCheckBox>

class NetGame : public Board
{
    Q_OBJECT
public:
    NetGame();
    void closeEvent ( QCloseEvent * e );

    //start
    //实现发送消息
    virtual void sendMsg();
    //end



    QWebSocket* _socket;
    QTimer *_ptimer;

    //执棋选择框
    QCheckBox *holdCheckBox;

    virtual void StartClick();

    void showButton();

    //重写点击事件
    void click(int id, int row, int col);
    //重写走棋回调
//    void isShowButton(bool isRed);
    //悔棋
    void regretPiece();

private slots:
    void onDisconnected();
    void onConnected();
    void onTextReceived(QString msg);
    void reconnect();

    //重写快捷消息双击
    virtual void quickDoubleClicked(const QModelIndex &index);

    //重写悔棋单击
    virtual void RegretClick();


private:
    QString wsUrl;
};

#endif // NETGAME_H
