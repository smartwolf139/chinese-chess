#ifndef CANJU_H
#define CANJU_H

#include <QDialog>
#include <QDebug>
#include <QFile>
#include <QMap>

#include <Board.h>
#include <SingleGame.h>

namespace Ui {
class CanJu;
}

class CanJu : public QDialog
{
    Q_OBJECT

public:
    explicit CanJu(QWidget *parent = 0);
    ~CanJu();

    //初始化残局库
    void initCanJu();
    //保存所有残局
    QMap<QString,QString> canJuMap;
    //选择的角色
    QString role;

private slots:
    void on_tableWidget_doubleClicked(const QModelIndex &index);

private:
    Ui::CanJu *ui;
};

#endif // CANJU_H
