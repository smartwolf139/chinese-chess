#ifndef BOARD_H
#define BOARD_H
#include <QWidget>
#include "Piece.h"
#include "Step.h"
#include "Skin.h"
#include <QPushButton>
#include <QTextEdit>
#include <QTimer>
#include <QCheckBox>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QListWidget>

#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QLabel>

class Board : public QWidget
{
    Q_OBJECT
public:
    explicit Board(QWidget *parent = nullptr);

    ~Board();
    void closeEvent ( QCloseEvent * e );


    //优化界面
    QPropertyAnimation *animation;
    QGraphicsOpacityEffect  *redOpacity;
    bool isPlay;
    //播放动画
    void playAnimation(QString name);
    //动画标签
    QLabel *lab;
    //动画定时器
    QTimer *timer;
    //聊天面板
    QPlainTextEdit *chatEdit;
    //快捷聊天面板
    QListWidget *quickChatList;
    //快捷聊天信息
    QStringList quickList;
    //聊天输入框
    QLineEdit *chatLine;
    //回车监听
    bool eventFilter(QObject *obj, QEvent *event);
    //发送消息到面板
    virtual void sendMsg();
    //是否播放音效
    void isPlaySound(QString content);
    //播放音乐
    void playSound(int flag);
    //男声女声控制
    QString isMan;
    //下棋位置框起来
    Step *lastStep;
    //end


    //是否胜利
    int isWin;

    //token
    QString globalToken;

    //是否开始
    bool isStart;
    //是否刷新界面
    bool isUpdate;
    //是否在下方
    bool _bSide;

    //绘制悔棋按钮
    QPushButton *regretButton;
    void showButton();
    //绘制开始按钮
    QPushButton *startButton;
    //绘制再来一局按钮
    QPushButton *againButton;
    //绘制日志窗口
    QTextEdit *logEdit;
    //绘制音乐按钮
    QCheckBox *musicCheckBox;

    //绘制棋盘
    void paintEvent(QPaintEvent *);
    //棋子
    Piece _p[32];
    //棋子直径
    int _d;
    //棋子是否选中
    int _selectid;
    //该谁走
    bool _bRedRun;
    //悔棋步数
    QVector<Step*> regretSteps;

    //绘制棋子
    void drawPiece(QPainter& painter,int id);
    //返回棋盘行列对应的坐标
    QPoint center(int x,int y);
    QPoint center(int id);

    //鼠标释放事件
    void mousePressEvent(QMouseEvent *event);
    //获取坐标的XY
    bool getXY(QPoint pt,int& x,int& y);
    //判断是否符合规则
    bool canMove(int moveid,int x,int y,int killid);
    bool canMoveJ(int moveid,int x,int y,int killid);
    bool canMoveS(int moveid,int x,int y,int killid);
    bool canMoveC(int moveid,int x,int y,int killid);
    bool canMoveM(int moveid,int x,int y,int killid);
    bool canMoveP(int moveid,int x,int y,int killid);
    bool canMoveX(int moveid,int x,int y,int killid);
    bool canMoveB(int moveid,int x,int y,int killid);
    //获取指定位置的棋子
    int getPieceId(int dr,int dc);
    //获取两颗棋子中间有多少颗棋子
    int getPieceCountByLine(int row1,int col1,int row2,int col2);
    //点击移动
    virtual void click(int id,int row,int col);
    //判断颜色是否相同
    bool sameColor(int moveid,int killid);

    //吃掉棋子
    void killPiece(int id);
    //复活棋子
    void revivePiece(int id);
    //移动棋子
    void movePiece(int moveid,int row,int col);
    //移动棋子
    void movePiece(int moveid,int killid,int row,int col);

    //保存能移动的步
    void saveStep(int moveid,int killid,int row,int col,QVector<Step*>& steps);
    //保存能移动的步
    //void saveStep(Step *step,QVector<Step*>& steps);

//    //悔棋保存
//    void saveRegretStep(int moveid,int killid,int row,int col,QVector<Step*>& steps);
    //是否在下方
    bool isBottomSide(int id);

    //改谁走棋的回调
    virtual void isShowButton(bool isRed);

    void qLog(QString msg);

    //着法
    QString getMoveName(Step *step);
    //添加到发送框
    void addMsg(QString content);
    //添加游戏结束
    void gameOver();

public slots:
    //再来一局
    virtual void AgainClick();

private slots:
    //悔棋点击
    virtual void RegretClick();
    //开始点击
    virtual void StartClick();
    //播放音乐
    virtual void MusicClick();
    //快捷消息双击
    virtual void quickDoubleClicked(const QModelIndex &index);
    //胜利动画
    void animationFinished();
    //定时器超时
    void handleTimeout();

signals:

};

#endif // BOARD_H
