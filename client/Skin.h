#ifndef SKIN_H
#define SKIN_H

#include <QString>

#include <QMediaPlaylist>
#include <QMediaPlayer>

class Skin
{
public:
    Skin();
    //全局皮肤
    static QString GlobalSkin;
    //音乐组件
    static QMediaPlaylist *Playlist;
    static QMediaPlayer *Music;
    static QMediaPlayer * eatPlayer;
    static QMediaPlayer * goPlayer;
    static QMediaPlayer * lsPlayer;

    static void Init();
};

#endif // SKIN_H
