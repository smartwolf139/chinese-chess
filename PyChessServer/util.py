import hashlib

import pymysql

def GetConn():
    '''
    获取一个数据库连接
    :return: 返回连接
    '''
    conn = pymysql.connect(host='127.0.0.1', user='root', password='123456', database='chess', cursorclass=pymysql.cursors.DictCursor)
    return conn


def GetMd5(text):
    '''
    获取MD5
    :param text: 要加密的数据
    :return:     md5值
    '''
    md5hash = hashlib.md5(text.encode(encoding='UTF-8'))
    return md5hash.hexdigest()
