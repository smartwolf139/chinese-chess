import threading
from flask import Flask, request

from service import Login, Register
from websocket import runWS, runHart

# 启动ws服务
threading.Thread(target=runWS).start()
threading.Thread(target=runHart).start()

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


@app.route('/login',methods=['GET', 'POST'])
def login():  # 登录接口
    data = request.json
    return Login(data)


@app.route('/register',methods=['GET', 'POST'])
def register():  # 注册接口
    data = request.json
    return Register(data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8765)
